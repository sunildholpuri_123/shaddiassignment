package com.shadimatech.com.util;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.shadimatech.com.R;


public class AnimationUtil {

    public static void animateItemRemove(Context context, View itemView){
        Animation animation= AnimationUtils.loadAnimation(context, R.anim.swipe_to_right);
        itemView.startAnimation(animation);
    }
}
