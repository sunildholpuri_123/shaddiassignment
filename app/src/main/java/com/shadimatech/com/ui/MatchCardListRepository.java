package com.shadimatech.com.ui;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.shadimatech.com.api.WebClient;
import com.shadimatech.com.api.WebService;
import com.shadimatech.com.model.ApiResponse;
import com.shadimatech.com.model.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatchCardListRepository {
    private WebService webClient;

    public MatchCardListRepository() {
        webClient = WebClient.getClient().create(WebService.class);
    }

    public LiveData<List<Result>> getMatchCardList() {
        final MutableLiveData<List<Result>> data = new MutableLiveData<>();

        Call<ApiResponse> call = webClient.getMatchCardList("10");
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body() != null) {
                    data.setValue(response.body().getResults());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, Throwable t) {
                Log.e("API FAILURE",t.getMessage());
            }
        });
        return data;
    }

}
