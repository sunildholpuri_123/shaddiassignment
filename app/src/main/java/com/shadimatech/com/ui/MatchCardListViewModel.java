package com.shadimatech.com.ui;



import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.shadimatech.com.model.Result;

import java.util.List;

public class MatchCardListViewModel extends ViewModel {
    private MatchCardListRepository repository;
    private LiveData<List<Result>> listLiveData;

    public MatchCardListViewModel() {
        repository = new MatchCardListRepository();
    }

    public LiveData<List<Result>> getMatchCardItems() {
        // Fetch data only when it's null
        if (listLiveData == null) {
            listLiveData = repository.getMatchCardList();
        }

        return listLiveData;
    }

    public void removeItemFromList(int position){
        if (listLiveData.getValue()!=null && listLiveData.getValue().size()>0){
            listLiveData.getValue().remove(position);

        }
    }
}
