package com.shadimatech.com.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.shadimatech.com.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // toolbar title
        getSupportActionBar().setTitle(getString(R.string.title));
        if (savedInstanceState == null) {
            // load the fragment for the first time
            MatchCardListFragment fragment = new MatchCardListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_container, fragment, MatchCardListFragment.TAG).commit();
        }
    }
}
