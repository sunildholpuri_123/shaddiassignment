package com.shadimatech.com.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.shadimatech.com.R;
import com.shadimatech.com.model.Result;

import java.util.List;

public class MatchCardListFragment extends Fragment implements MatchCardListAdapter.UserListAdapterClickListener {

    public static final String TAG = MatchCardListFragment.class.getSimpleName();

    private MatchCardListViewModel viewModel;
    private Context context;


    private MatchCardListAdapter matchCardListAdapter;
    RecyclerView recyclerView;

    LinearLayout progressLayout;


    private MatchCardListAdapter.UserListAdapterClickListener UserListAdapterClickListener;

    public MatchCardListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_list, container, false);
        recyclerView =view.findViewById(R.id.user_list);
        progressLayout =view.findViewById(R.id.progress_bar);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        // getting ViewModel instance
        showProgress();
        setUpRecyclerView();
        viewModel = ViewModelProviders.of(getActivity()).get(MatchCardListViewModel.class);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Observing to list items
        viewModel.getMatchCardItems().observe(getActivity(), new Observer<List<Result>>() {
            @Override
            public void onChanged(@Nullable List<Result> results) {
                // displaying item in recycler view
                matchCardListAdapter.setItems(results);
                hideProgress();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }

    private void setUpRecyclerView() {
        UserListAdapterClickListener = this;
        matchCardListAdapter = new MatchCardListAdapter(context,UserListAdapterClickListener);
        recyclerView.setAdapter(matchCardListAdapter);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
    }
    private void showProgress(){
        progressLayout.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private void hideProgress(){
        progressLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }
    @Override
    public void onItemClick(int position) {
           viewModel.removeItemFromList(position);
    }
}
