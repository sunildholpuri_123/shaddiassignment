package com.shadimatech.com.ui;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.shadimatech.com.R;
import com.shadimatech.com.model.Result;
import com.shadimatech.com.util.AnimationUtil;

import java.util.ArrayList;
import java.util.List;


public class MatchCardListAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Result> resultList;
    private final int ANIMATION_DURATION = 300;
    private UserListAdapterClickListener userListAdapterClickListener;
    final public static int ViewTypeListRow = 1, ViewTypeNoData = 2;

    public MatchCardListAdapter(Context context, UserListAdapterClickListener userListAdapterClickListener) {
        this.context = context;
        this.userListAdapterClickListener = userListAdapterClickListener;
        resultList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.user_list_row, viewGroup, false);
//        return new UserListViewHolder(view);

        View view;
        switch (viewType) {
            case ViewTypeListRow:
                view = LayoutInflater.from(context).inflate(R.layout.user_list_row, viewGroup, false);
                return new UserListViewHolder(view);
            case ViewTypeNoData:
                view = LayoutInflater.from(context).inflate(R.layout.user_list_no_data_row, viewGroup, false);
                return new NoDataAvailableViewHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder!=null && viewHolder instanceof UserListViewHolder) {
            final UserListViewHolder matchCardListViewHolder = (UserListViewHolder) viewHolder;
            matchCardListViewHolder.bind(resultList.get(position));
            matchCardListViewHolder.btnLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemRemove(position, matchCardListViewHolder);
                }
            });
            matchCardListViewHolder.btnSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemRemove(position, matchCardListViewHolder);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (resultList.size()==0){
            return ViewTypeNoData;
        }else {
            return ViewTypeListRow;
        }
    }

    @Override
    public int getItemCount() {
        if(resultList.size() == 0){
            return 1;
        }else {
            return resultList.size();
        }
    }

    public class UserListViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView age;
        ImageView image;
        FloatingActionButton btnSkip;
        FloatingActionButton btnLike;

        public UserListViewHolder(View itemView) {
            super(itemView);
            btnSkip = itemView.findViewById(R.id.skip_button);
            btnLike = itemView.findViewById(R.id.like_button);
            image = itemView.findViewById(R.id.item_image);
            name = itemView.findViewById(R.id.item_name);
            age = itemView.findViewById(R.id.item_age);
        }

        void bind(Result result) {
            name.setText(result.getName().getFirst() + " " + result.getName().getLast());
            age.setText(result.getRegistered().getAge() + " yrs");
            Glide.with(context)
                    .load(result.getPicture().getLarge())
                    .into(image);
        }


    }
    public class NoDataAvailableViewHolder extends RecyclerView.ViewHolder {

        public NoDataAvailableViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public void setItems(List<Result> items) {
        resultList.clear();
        resultList.addAll(items);
        notifyDataSetChanged();
    }

    public void removeItemAtPosition(int position){
        resultList.remove(position);
        notifyDataSetChanged();
    }

    private void onItemRemove(final int position, final UserListViewHolder matchCardListViewHolder) {
        AnimationUtil.animateItemRemove(context, matchCardListViewHolder.itemView);
        matchCardListViewHolder.btnSkip.setEnabled(false);
        matchCardListViewHolder.btnLike.setEnabled(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                matchCardListViewHolder.btnSkip.setEnabled(true);
                matchCardListViewHolder.btnLike.setEnabled(true);
                removeItemAtPosition(position);
                userListAdapterClickListener.onItemClick(position);
            }
        }, ANIMATION_DURATION);
    }

    public interface UserListAdapterClickListener {
         void onItemClick(int position);
    }
}
