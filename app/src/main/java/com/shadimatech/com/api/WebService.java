package com.shadimatech.com.api;


import com.shadimatech.com.model.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebService {

    @GET("api/")
    Call<ApiResponse> getMatchCardList(@Query("results") String results);

}
